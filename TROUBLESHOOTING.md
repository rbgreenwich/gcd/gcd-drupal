# Troubleshooting

For general Drupal troubleshooting, the wiki might be useful: 

[https://gitlab.com/rbgreenwich/dev/documentation/-/wikis/Updating-Drupal](https://gitlab.com/rbgreenwich/dev/documentation/-/wikis/Updating-Drupal)

Please contribute troubleshooting steps for any issues you encounter.

## Container network problems

As detailed in [This StackOverflow question](https://stackoverflow.com/questions/20430371/my-docker-container-has-no-internet/45644890#45644890), the container's DNS settings (defined in /etc/resolv.conf) may be faulty. You can manually define the DNS for all the hosts' containers by editing /etc/docker/daemon.json:

```
{
    "dns": ["8.8.8.8"]
}
```

8.8.8.8 in this case is a DNS server hosted by Google, but others should work.

## Outpost services data migration fails

The Drupal wesite runs a regular migration command on cron to 
migrated the data from the Outpost api into the Drupal services nodes. 

Sometimes this fails. 

To check, connect to the container and use drush to inspect the migrations.

```
drush ms 
```

If all is healthy, it will probably show something like this.

```
 ------------------------------------------------------------ -------- ------- -------------- ------------- ---------------------
  Migration ID                                                 Status   Total   Imported       Unprocessed   Last Imported
 ------------------------------------------------------------ -------- ------- -------------- ------------- ---------------------
  outpost_migration_deriver:localgov_outpost_category_1        Idle     62      62 (100%)      0             2024-10-01 08:03:36
  outpost_migration_deriver:localgov_outpost_contacts_1        Idle     957     987 (103.1%)   -30           2024-10-01 08:04:18
  outpost_migration_deriver:localgov_outpost_locations_1       Idle     746     764 (102.4%)   -18           2024-10-01 08:04:54
  outpost_migration_deriver:localgov_outpost_services_1        Idle     925     925 (100%)     0             2024-10-01 08:06:41
  outpost_migration_deriver:localgov_outpost_suitabilities_1   Idle     7       7 (100%)       0             2024-10-01 08:10:26
 ------------------------------------------------------------ -------- ------- -------------- ------------- ---------------------
 ```

 Sometimes one migration will get stuck, for example: 

 ```
 


www-data@gcd-website-deploy-7d58f7576b-hgvf8:/opt/drupal/localgov-dist$ drush ms
------------------------------------------------------------ ----------- ------- ---------------- ------------- ---------------------  
Migration ID                                                 Status      Total   Imported         Unprocessed   Last Imported
------------------------------------------------------------ ----------- ------- ---------------- ------------- ---------------------  
outpost_migration_deriver:localgov_outpost_category_1        Idle        4       62 (1550%)       -58           2024-09-30 11:03:50  
outpost_migration_deriver:localgov_outpost_contacts_1        Idle        4       955 (23875%)     -951          2024-09-30 11:03:52  
outpost_migration_deriver:localgov_outpost_locations_1       Importing   3       728 (24266.7%)   -725          2024-09-27 12:32:35  
outpost_migration_deriver:localgov_outpost_services_1        Idle        4       2 (50%)          2  
outpost_migration_deriver:localgov_outpost_suitabilities_1   Idle        0       7                -7            2024-09-30 11:07:13
------------------------------------------------------------ ----------- ------- ---------------- ------------- ---------------------
 ```

In this case, the `localgov_outpost_locations_1` migration is stuck.

We need to reset it and run all migrations again.

Reset the migration to idle:
```
drush mrs localgov_outpost_locations_1
```

Run all migrations again 

```
drush mim --all --update
```

### Run cron manually on Outpost UI container.

The Oupost json api endpoint is at https://outpost-api.prd.royalgreenwich.gov.uk/api/v1/services

The data here is populated by a cron job that runs periodically to copy published data to a mongodb database to feed the api endpoint. 

At times the cron job has failed, so it can be useful to trigger this manually.

If we SSH into the outpost-ui-deploy container we can run 

```
/exec bin/rails build_public_index 
```
This is the same command that the cronjob is running. 

