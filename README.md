# Greenwich Community Directory - Drupal site

A Composer-based installer based on the LocalGov Drupal Composer project
template for the LocalGov Drupal distribution.

## Design system 

The Drupal theme is built to integrate tightly with the Story Book display of the design system.

https://rbgreenwich.gitlab.io/dev/main-website/lgd-main-website/?path=/story/welcome--page

## Quick start

### 1 Clone the respositories

#### 1.1 Clone this repository

```sh
git clone git@gitlab.com:rbgreenwich/gcd/gcd-drupal.git
```

#### 1.2 Clone this mainsite repository if it doesn't already exist.

The mainsite is needed to do any frontend development work and is symlinked in
to the web container of the GCD project. Without this ddev will not come up.
If you already have the main site locally you can skip this step.

```sh
git clone git@gitlab.com:rbgreenwich/dev/main-website/lgd-main-website.git
```

#### 1.3 Change directory to the GCD root

```sh
cd gcd-drupal
```

### 2. Download a copy of the Drupal database.

Currently easiest to log into the dev or prod site and use Backup Migrate to
download a copy of the database.

https://gcd.dev.royalgreenwich.gov.uk/admin/config/development/backup_migrate

### 3. Use DDEV to run local webserver.

#### 3.1 Create the required .env file

Copy the ddev .env.example file and modify the value of `MAIN_SITE_ROOT` to point
to the main site root directory on your host system.

```sh
cp .ddev/.env.example .ddev/.env
```

#### 3.2 Bring up the containers

Boot up DDEV, run composer install and import the database and latest config.

```sh
ddev start # Start ddev.
ddev composer install # Install all required code packages with composer.
ddev import-db -f database.sql.gz # Import the database with DDEV.
ddev drush cr # Cache rebuild, using drush.
ddev drush updb # Run database updates with Drush.
ddev drush cim # Import the config, with Drush.
ddev drush uli # Generate a one time log in link, with Drush.
```

Note, there is also a helper command that will run the equivalent of the above comands:

```sh
ddev init_drupal_instance database.sql
```

#### 3.3 Disable GTM

It is important to disable the GTM container on your local site. To do this you
will need to create a `settings.local.php` file in `web/sites/default/` which
should contain this:

```php
// Disable GTM on local dev environment.
$config['google_tag.container.G-CZKCC8TWY2.65cb4f7f194f87.76805813']['status'] = 0;
```

If you wish to work on the greenwich_base theme - see below for instructions on configuring that setup

# Restoring from dev

Open the dev environment and create a **quick backup**. `admin > configuration > development > backup and migrate`.

```sh
# ensure your environment is up to date

git checkout develop

git pull

# replace current db database with the a new one
ddev import-db --no-progress --file=backup8.mysql

ddev drush cr

# import any local changes
ddev drush cim

# because it never hurts to clear the cache!
ddev drush cr

```

# Enabling theme development

Theme development is optionally enabled, when not enabled the `base_theme` is sourced from the gitlab [greenwich_base](https://gitlab.com/rbgreenwich/packages/greenwich_base) package, which is built from the [design system](https://gitlab.com/rbgreenwich/design_system).

The theme development mode replaces the greenwich_base theme with symlinks to
the main site's theme. This means that when developing the base theme it must
be done in the main site's repo.

```sh
# Set up symlinks to the main site.
ddev theme-dev
```

# Gitpod

We should also be able to start this in Gitpod.

[Open in Gitpod](https://gitpod.io/#https://gitlab.com/rbgreenwich/gcd/gcd-drupal/)

# Usage

This repository comes with the root composer file which attempts to define all
code dependencies. Cloning the repo and running comploser install should bring
all code up to the same point.

We also have configuration files for DDEV, to help with a fast
and consistent local development setup.

You can also optionally enable the greenwich_base theme if you need to work on it. Read below for instructions on how to enable this.

### Import a database

Then it is super useful to import a copy of the databse so that we can import
updated configuration for the site and collaborate with other developers.

For now we can start with this.

```sh
wget database.sql
ddev drush sqlc < database.sql
ddev drush cr
ddev drush updb -y
```

### Import configs

Import any new config.

```sh
ddev drush cim
```

(If required export any config before committing your work)

```sh
ddev drush cim
```

### Clear cache

Your most used command...

Now clear the cache and generate a log in link.

```sh
ddev drush cr
```

### Generate admin login link

No passwords needed!

```sh
ddev drush uli
```

### Launch the site

And always rebuild the cache!

## Gitlab tokens to require private packages or repos.

Some packages or repos required by the root composer project might be private.
With Gitlab we need to generate personal access tokens to allow composer to
access the wothout further authentication.

To do this first generate a personal access token with at least api and read_api
permisssion. For example, follow this link:

https://gitlab.com/-/profile/personal_access_tokens?name=Example+Access+token&scopes=api,read_api

Once you've created the access token, copy it down as you won't be able to see
it again.

Then we can configure composer inside the ddev container to us it with:

```
ddev composer config --global --auth gitlab-token.gitlab.com {you personal acceses token}
```

## Branching methodology and deploying to the dev site

We are aiming to use the 'release flow' branching methodology.

See: http://releaseflow.org/

For now, we are using `develop` as the mainline branch.

So for a new feature, we might start by creating an issue in Gitlab.

For example, when adding the Backup Migrate module and configuration, we created the issue https://gitlab.com/rbgreenwich/gcd/gcd-drupal/-/issues/2

We then create a feature branch following the naming convention feature/[issue number]-[description] `feature/2-backup-migrate`.

```
git checkout develop
git pull origin develop
git checkout -b feature/2-backup-migrate
```

Locally we committed changes to code and config.

```
ddev composer require drupal/backup-migrate
ddev drush en backup-migrate
ddev drush cex
git status
git add config/ composer.json composer.lock
git commit -m 'Add backup_migrate module for developers.'
git push origin feature/2-backup-migrate
```

Gitlab then provides and handy link to generate the merge request.

Either use this link or manually create a merge request for the feature branch back into the develop branch.

Once any needed review has taken place, merge the merge request into develop.

This will trigger the deploy to the dev site.

If it doesn't work, ask Chris on Teams.

# Troubleshooting / debugging

See [TROUBLESHOOTING.md](./TROUBLESHOOTING.md)

# TODO

- [ ] https://medium.com/packagist/custom-package-definitions-3f433629861e
- [ ] https://stackoverflow.com/questions/76530637/gitlab-how-to-include-build-artifacts-when-adding-a-package-via-ci-pipeline-to
- [ ] https://stackoverflow.com/questions/29013457/how-to-store-releases-binaries-in-gitlab
