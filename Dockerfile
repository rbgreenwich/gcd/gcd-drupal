FROM drupal:10.3.6-php8.3-apache-bookworm

ARG SITE_NAME

# Set the Apache site config to include PHP environment variables
COPY assets/apache/site.conf /etc/apache2/sites-enabled/000-default.conf

# Use the default production configuration for PHP
RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

# Override some PHP configuration with environment variables set by Kubernetes.
COPY assets/php/override-env-php.ini /usr/local/etc/php/conf.d/override-env-php.ini

# Set PHP max memory config value to unlimited
RUN sed -i 's/memory_limit = 128M/memory_limit = -1/g' /usr/local/etc/php/php.ini

# Enable mysqli extension
RUN docker-php-ext-install mysqli && docker-php-ext-enable mysqli

# Set non-privileged port for apache
RUN sed -i 's/Listen 80/Listen 8080/g' /etc/apache2/ports.conf

# Install APCu extensioon for PHP
RUN pecl install apcu-5.1.23 && \
    docker-php-ext-enable apcu

# Install OS packages
RUN rm /bin/sh && ln -s /bin/bash /bin/sh && \
    apt-get update && apt-get install --no-install-recommends -y \
    curl \
    wget \
    telnet \
    vim \
    ssh \
    git \
    zip \
    unzip \
    mariadb-client \
    nano \
    awscli

# Clean cache for OS package repository
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

# Install Composer & Drush
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
    ln -s /root/.composer/vendor/bin/drush /usr/local/bin/drush

# Add composer bin path to ENV to support running drush
ENV PATH="${PATH}:/opt/drupal/localgov-dist/bin"

# Create site directory
RUN mkdir ${SITE_NAME}

# Copy across repository files and folders
COPY --chown=www-data:www-data . ${SITE_NAME}/.

# Set www-data as owner of apache directory
RUN chown -R www-data /etc/apache2

# Set www-data as owner of PHP directory
RUN chown -R www-data /usr/local/etc/php

# Set www-data as owner of site directory
RUN chown -R www-data:www-data ${SITE_NAME}

# Set www-data as owner of its home directory /var/www
RUN chown -R www-data /var/www

WORKDIR ${SITE_NAME}

COPY --chown=www-data:www-data --chmod=600 assets/docker/ssh /var/www/.ssh

# Run following commands as www-data user (apache)
USER www-data

# Use Composer to create localgov-drupal drupal distributiuon configured to Royal Borough of Greenwich requirements
# Override COMPOSER_PROCESS_TIMEOUT ( defaults to 300 )
RUN export COMPOSER_PROCESS_TIMEOUT=600
RUN composer install --no-dev

# Ensure www-data owns default site directory
RUN chown -R www-data:www-data web/sites/default

# Remove settings.php as to ensure docker-entrypoint.sh performs a site-install
# This will be the default behaviour unless an existing site is mounted via a network share
RUN rm web/sites/default/settings.php
RUN rm web/sites/default/default.settings.php

EXPOSE 80

COPY assets/docker/docker-entrypoint.sh /opt/docker-entrypoint.sh

ENTRYPOINT /opt/docker-entrypoint.sh

