<?php

namespace Drupal\greenwich_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a global site footer block.
 *
 * @Block(
 *   id = "greenwich_site_footer",
 *   admin_label = @Translation("Greenwich: Site Footer"),
 *   category = @Translation("Greenwich Blocks")
 * )
 */
class GreenwichFooterBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#markup' => '<h2>Greenwich Footer Block</h2>',
    ];
  }

}
