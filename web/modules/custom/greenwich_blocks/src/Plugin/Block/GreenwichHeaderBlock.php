<?php

namespace Drupal\greenwich_blocks\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Menu\MenuLinkTreeInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Menu\MenuTreeParameters;

/**
 * Provides a global site header block.
 *
 * @Block(
 *   id = "greenwich_site_header",
 *   admin_label = @Translation("Greenwich: Site Header"),
 *   category = @Translation("Greenwich Blocks")
 * )
 */
class GreenwichHeaderBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  protected $menuLinkTree;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config_factory, MenuLinkTreeInterface $menu_link_tree) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->configFactory = $config_factory;
    $this->menuLinkTree = $menu_link_tree;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('menu.link_tree')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $settings = $this->getConfiguration();
    $site_config = $this->configFactory->get('system.site');

    return [
      '#theme' => 'header',
      '#content' => 'test content',
      '#variant' => $settings['variant'] ?? 'website',
      '#title' => $settings['title'] ?? $site_config->get('name'),
      '#url' => $settings['url'] ?? $site_config->get('page.front', 'node'),
      '#menu_main' => $this->getMenuOutput($settings['menu_main'] ?? 'main') ?? NULL,
      '#menu_aside' => $this->getMenuOutput($settings['menu_aside'] ?? 'header-aside') ?? NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $config['title'] ?? NULL,
      '#description' => $this->t('Leave blank to use the same as the page title in <a href="@basic-site-settings">basic site settings</a>', ['@basic-site-settings' => '/admin/config/system/site-information']),
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Url'),
      '#default_value' => $config['url'] ?? NULL,
      '#description' => $this->t('Leave blank to use the frontpage setting defined in <a href="@basic-site-settings">basic site settings</a>', ['@basic-site-settings' => '/admin/config/system/site-information']),
    ];

    $form['variant'] = [
      '#type' => 'select',
      '#title' => $this->t('Variant'),
      '#options' => ['website' => $this->t('Website'), 'gcd' => $this->t('Greenwich community directory')],
      '#default_value' => $config['variant'] ?? 'website',
      '#description' => $this->t('Choose from preconfigured options for the header'),
    ];

    $form['menu_main'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select the main menu'),
      '#default_value' => $config['menu_main'] ?? 'main',
      '#description' => $this->t('The name of your main menu, defaults to [main]'),
    ];

    $form['menu_aside'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Select the aside menu'),
      '#default_value' => $config['menu_aside'] ?? 'header-aside',
      '#states' => [
        'visible' => [
          ':input[name="settings[variant]"]' => ['value' => 'website'],
        ],
      ],
      '#description' => $this->t('The name of the right hand side menu, defaults to `header_aside`'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);

    // Save the block-specific settings.
    $this->configuration['title'] = $form_state->getValue('title') == '' ? NULL : $form_state->getValue('title');
    $this->configuration['url'] = $form_state->getValue('url') == '' ? NULL : $form_state->getValue('url');
    $this->configuration['variant'] = $form_state->getValue('variant');
    $this->configuration['menu_main'] = $form_state->getValue('menu_main') == '' ? NULL : $form_state->getValue('menu_main');
    $this->configuration['menu_aside'] = $form_state->getValue('menu_services') == '' ? NULL : $form_state->getValue('menu_services');
  }

  /**
   * Retrieves the output of a menu.
   *
   * @param string $menu_name
   *   The name of the menu.
   *
   * @return array
   *   The menu render array.
   */
  protected function getMenuOutput($menu_name) {
    $parameters = new MenuTreeParameters();
    $parameters->setMaxDepth(2);

    $tree = $this->menuLinkTree->load($menu_name, $parameters);
    $manipulators = [
      ['callable' => 'menu.default_tree_manipulators:checkAccess'],
      ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
    ];
    $tree = $this->menuLinkTree->transform($tree, $manipulators);
    return $this->menuLinkTree->build($tree);
  }

}
