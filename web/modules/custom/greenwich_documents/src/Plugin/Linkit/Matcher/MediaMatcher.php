<?php

namespace Drupal\greenwich_documents\Plugin\Linkit\Matcher;

use Drupal\linkit\Plugin\Linkit\Matcher\EntityMatcher;
use Drupal\Core\Entity\Query\QueryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\media\MediaInterface;
use Drupal\Component\Utility\Html;

/**
 * Provides specific linkit matchers for the media entity type.
 *
 * @Matcher(
 *   id = "entity:media",
 *   label = @Translation("Media"),
 *   target_entity = "media",
 *   provider = "media"
 * )
 */
class MediaMatcher extends EntityMatcher {

  /**
   * {@inheritdoc}
   */
  protected function buildEntityQuery($search) {
    $query = parent::buildEntityQuery($search);
    $search = $this->database->escapeLike($search);

    // Remove the existing condition on the 'name' field.
    $this->removeExistingCondition($query, 'name');

    // Search in default fields.
    $or = $query->orConditionGroup()
      ->condition('name', '%' . $search . '%', 'LIKE')
      ->condition('title', '%' . $search . '%', 'LIKE');

    $query->condition($or);

    return $query;
  }

  /**
   * Removes an existing condition from the query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query object.
   * @param string $field
   *   The field name of the condition to remove.
   */
  protected function removeExistingCondition(QueryInterface $query, $field) {
    $conditions = &$this->getQueryConditions($query);
    foreach ($conditions as $key => $condition) {
      if ($condition['field'] == $field) {
        unset($conditions[$key]);
      }
    }
  }

  /**
   * Gets the conditions array from the query.
   *
   * @param \Drupal\Core\Entity\Query\QueryInterface $query
   *   The query object.
   *
   * @return array
   *   Reference to the conditions array.
   */
  protected function &getQueryConditions(QueryInterface $query) {
    $reflection = new \ReflectionObject($query);
    $condition_property = $reflection->getProperty('condition');
    $condition_property->setAccessible(TRUE);
    $condition = $condition_property->getValue($query);
    return $condition->conditions();
  }

  /**
   * {@inheritdoc}
   */
  protected function buildLabel(EntityInterface $entity) {
    $label = parent::buildLabel($entity);

    if ($entity instanceof MediaInterface && $entity->hasField('title') && !$entity->get('title')->isEmpty()) {
      $field_title = $entity->get('title')->value;
      $label = $field_title . ' (' . $label . ')';
    }

    return Html::escape($label);
  }

}
