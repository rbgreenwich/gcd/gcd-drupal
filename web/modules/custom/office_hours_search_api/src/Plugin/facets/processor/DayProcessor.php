<?php

namespace Drupal\office_hours_search_api\Plugin\facets\processor;

use Drupal\Core\Cache\UnchangingCacheableDependencyTrait;
use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\PostQueryProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Drupal\office_hours\OfficeHoursDateHelper;

/**
 * Replaces facet values for integer to day.
 *
 * @FacetsProcessor(
 *   id = "office_hours_day",
 *   label = @Translation("Office hours: Days"),
 *   description = @Translation("Map days of the week for office hours day value."),
 *   stages = {
 *     "post_query" = 50,
 *   },
 * )
 */
class DayProcessor extends ProcessorPluginBase implements PostQueryProcessorInterface {

  use UnchangingCacheableDependencyTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'day_format' => 'long',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet) {
    $build = [];
    $build['day_format'] = [
      '#title' => $this->t('Weekday notation'),
      '#type' => 'select',
      '#options' => [
        'long' => $this->t('long'),
        'short' => $this->t('3-letter weekday abbreviation'),
        'two_letter' => $this->t('2-letter weekday abbreviation'),
        'number' => $this->t('number'),
        'none' => $this->t('none'),
      ],
      '#default_value' => $this->getConfiguration()['day_format'],
    ];
    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function postQuery(FacetInterface $facet) {
    $day_names = OfficeHoursDateHelper::weekDays($this->getConfiguration()['day_format']);

    foreach ($facet->getResults() as $result) {
      $raw_value = $result->getRawValue();
      $result->setDisplayValue($day_names[$raw_value]);
    }
  }

}
