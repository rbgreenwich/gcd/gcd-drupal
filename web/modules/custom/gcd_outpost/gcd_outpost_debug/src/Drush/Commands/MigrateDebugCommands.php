<?php

namespace Drupal\gcd_outpost_debug\Drush\Commands;

use Consolidation\OutputFormatters\StructuredData\RowsOfFields;
use Drupal\Core\Utility\Token;
use Drupal\gcd_outpost_debug\Drupal\Migrate\MigrateExecutable;
use Drupal\migrate\Plugin\MigrationInterface;
use Drush\Attributes as CLI;
use Drush\Commands\core\DocsCommands;
use Drush\Commands\DrushCommands;
use Drush\Commands\core\MigrateRunnerCommands;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Path;

/**
 * A Drush commandfile.
 *
 * In addition to this file, you need a drush.services.yml
 * in root of your module, and a composer.json file that provides the name
 * of the services file to use.
 */
final class MigrateDebugCommands extends MigrateRunnerCommands {

  /**
   * Perform one or more migration processes.
   *
   * @throws \Exception
   *   When not enough options were provided or no migration was found.
   */
  #[CLI\Command(name: 'migrate:import-debug', aliases: ['mimd', 'migrate-import-debug'])]
  #[CLI\Argument(name: 'migrationIds', description: 'Comma-separated list of migration IDs.')]
  #[CLI\Option(name: 'all', description: 'Process all migrations')]
  #[CLI\Option(name: 'tag', description: 'A comma-separated list of migration tags to import')]
  #[CLI\Option(name: 'limit', description: 'Limit on the number of items to process in each migration')]
  #[CLI\Option(name: 'feedback', description: 'Frequency of progress messages, in items processed')]
  #[CLI\Option(name: 'idlist', description: "Comma-separated list of IDs to import. As an ID may have more than one column, concatenate the columns with the colon ':' separator")]
  #[CLI\Option(name: 'update', description: 'In addition to processing unprocessed items from the source, update previously-imported items with the current data')]
  #[CLI\Option(name: 'force', description: 'Force an operation to run, even if all dependencies are not satisfied')]
  #[CLI\Option(name: 'execute-dependencies', description: 'Execute all dependent migrations first')]
  #[CLI\Option(name: 'timestamp', description: 'Show progress ending timestamp in progress messages')]
  #[CLI\Option(name: 'total', description: 'Show total processed item number in progress messages')]
  #[CLI\Option(name: 'progress', description: 'Show progress bar')]
  #[CLI\Option(name: 'delete', description: 'Delete destination records missed from the source. Not compatible with <info>--limit</info> and <info>--idlist</info> options, and high_water_property source configuration key.')]
  #[CLI\Usage(name: 'migrate:import-debug --all', description: 'Perform all migrations')]
  #[CLI\Usage(name: 'migrate:import-debug --all --no-progress', description: 'Perform all migrations but avoid the progress bar')]
  #[CLI\Usage(name: 'migrate:import-debug --tag=user,main_content', description: 'Import all migrations tagged with <info>user</info> and <info>main_content</info> tags')]
  #[CLI\Usage(name: 'migrate:import-debug classification,article', description: 'Import new terms and nodes using migration <info>classification</info> and <info>article</info>')]
  #[CLI\Usage(name: 'migrate:import-debug user --limit=2', description: 'Import no more than 2 users using the <info>user</info> migration')]
  #[CLI\Usage(name: 'migrate:import-debug user --idlist=5', description: 'Import the user record with source ID 5')]
  #[CLI\Usage(name: 'migrate:import-debug node_revision --idlist=1:2,2:3,3:5', description: 'Import the node revision record with source IDs [1,2], [2,3], and [3,5]')]
  #[CLI\Usage(name: 'migrate:import-debug user --limit=50 --feedback=20', description: 'Import 50 users and show process message every 20th record')]
  #[CLI\Usage(name: 'migrate:import-debug --all --delete', description: 'Perform all migrations and delete the destination items that are missing from source')]
  #[CLI\Topics(topics: [DocsCommands::MIGRATE])]
  #[CLI\ValidateModulesEnabled(modules: ['migrate'])]
  #[CLI\Version(version: '10.4')]
  public function import(?string $migrationIds = null, array $options = ['all' => false, 'tag' => self::REQ, 'limit' => self::REQ, 'feedback' => self::REQ, 'idlist' => self::REQ, 'update' => false, 'force' => false, 'execute-dependencies' => false, 'timestamp' => false, 'total' => false, 'progress' => true, 'delete' => false]): void {
    $tags = $options['tag'];
    $all = $options['all'];

    if (!$all && !$migrationIds && !$tags) {
        throw new \Exception(dt('You must specify --all, --tag or one or more migration names separated by commas'));
    }

    if (!$list = $this->getMigrationList($migrationIds, $options['tag'])) {
        throw new \Exception(dt('No migrations found.'));
    }

    $userData = [
        'options' => array_intersect_key($options, array_flip([
            'limit',
            'feedback',
            'idlist',
            'update',
            'force',
            'timestamp',
            'total',
            'progress',
            'delete',
        ])),
        'execute_dependencies' => $options['execute-dependencies'],
    ];

    // Include the file providing a migrate_prepare_row hook implementation.
    require_once Path::join($this->config->get('drush.base-dir'), 'src/Drupal/Migrate/migrate_runner.inc');
    // If the 'migrate_prepare_row' hook implementations are already cached,
    // make sure that system_migrate_prepare_row() is picked-up.
    \Drupal::moduleHandler()->resetImplementations();

    foreach ($list as $migrations) {
        array_walk($migrations, [static::class, 'executeMigration'], $userData);
    }
  }

  /**
   * Executes a single migration.
   *
   * If the --execute-dependencies option was given, the migration's
   * dependencies will also be executed first.
   *
   * @param MigrationInterface $migration
   *   The migration to execute.
   * @param string $migrationId
   *   The migration ID (not used, just an artifact of array_walk()).
   * @param array $userData
   *   Additional data passed to the callback.
   *
   * @throws \Exception
   *   If there are failed migrations.
   */
  protected function executeMigration(MigrationInterface $migration, string $migrationId, array $userData): void
  {
      static $executedMigrations = [];

      if ($userData['execute_dependencies']) {
          $dependencies = $migration->getMigrationDependencies()['required'];
          // Remove already executed migrations.
          $dependencies = array_diff($dependencies, $executedMigrations);
          if ($dependencies) {
              $requiredMigrations = $this->migrationPluginManager->createInstances($dependencies);
              array_walk($requiredMigrations, [static::class, __FUNCTION__], $userData);
          }
      }
      if (!empty($userData['options']['force'])) {
          $migration->set('requirements', []);
      }
      if (!empty($userData['options']['update'])) {
          if (empty($userData['options']['idlist'])) {
              $migration->getIdMap()->prepareUpdate();
          } else {
              $sourceIdValuesList = MigrateUtils::parseIdList($userData['options']['idlist']);
              $keys = array_keys($migration->getSourcePlugin()->getIds());
              foreach ($sourceIdValuesList as $sourceIdValues) {
                  $migration->getIdMap()->setUpdate(array_combine($keys, $sourceIdValues));
              }
          }
      }

      $executable = new MigrateExecutable($migration, $this->getMigrateMessage(), $this->output(), $userData['options']);
      // drush_op() provides --simulate support.
      drush_op([$executable, 'import']);
      if ($count = $executable->getFailedCount()) {
          // Nudge Drush to use a non-zero exit code.
          throw new \Exception(dt('!name migration: !count failed.', ['!name' => $migrationId, '!count' => $count]));
      }

      // Keep track of executed migrations.
      $executedMigrations[] = $migrationId;
  }

}
