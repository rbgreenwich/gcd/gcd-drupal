<?php

namespace Drupal\gcd_outpost_debug\Drupal\Migrate;

use Drupal\migrate\Event\MigrateImportEvent;
use Drupal\migrate\Plugin\MigrationInterface;
use Drush\Drupal\Migrate\MigrateExecutable as DrushMigrateExecutable;
use Drush\Drupal\Migrate\MigrateMissingSourceRowsEvent;

class MigrateExecutable extends DrushMigrateExecutable {

  /**
   * {@inheritdoc}
   */
  public function onPreImport(MigrateImportEvent $event): void {
    $migration = $event->getMigration();
    $id = $migration->id();

    $migrateLastImportedStore = \Drupal::keyValue('migrate_last_imported');
    $last_imported = intval($migrateLastImportedStore->get($id) / 1000);

    \Drupal::logger('system')->debug('Migration: Start %id last imported %date', [
      '%id' => $id,
      '%date' => date('Y-m-d H:i:m', $last_imported),
    ]);

    parent::onPreImport($event);
  }

  /**
   * {@inheritdoc}
   */
  public function onPostImport(MigrateImportEvent $event): void {
    parent::onPostImport($event);

    $migration = $event->getMigration();
    $id = $migration->id();

    \Drupal::logger('system')->debug('Migration: End %id now imported %date', [
      '%id' => $id,
      '%date' => date('Y-m-d H:i:m'),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  protected function handleMissingSourceRows(MigrationInterface $migration): void {
    $id = $migration->id();
    $idMap = $migration->getIdMap();
    $idMap->rewind();

    // Collect the destination IDs no more present in source.
    $destinationIds = [];
    $mapIds = 0;
    while ($idMap->valid()) {
      $mapIds++;
      $mapSourceId = $idMap->currentSource();
      if (!in_array($mapSourceId, $this->allSourceIdValues)) {
        $destinationIds[] = $idMap->currentDestination();
      }
      $idMap->next();
    }

    \Drupal::logger('system')->debug('Migration: Missing rows handler %id seen %allIds of %mapIds would remove %destIds', [
      '%id' => $id,
      '%allIds' => count($this->allSourceIdValues),
      '%mapIds' => $mapIds,
      '%destIds' => count($destinationIds),
    ]);

    if ($destinationIds) {
        $missingSourceEvent = new MigrateMissingSourceRowsEvent($migration, $destinationIds);
        $this->getEventDispatcher()->dispatch($missingSourceEvent);
    }
  }

}
