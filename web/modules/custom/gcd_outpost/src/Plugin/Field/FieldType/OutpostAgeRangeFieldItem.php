<?php

namespace Drupal\gcd_outpost\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\StringItem;


/**
 * Calculated age range field values.
 *
 * Values are populated by the list handler.
 *
 * @see \Drupal\gcd_outpost\Plugin\Field\OutpostAgeRangeFieldItemList
 *
 * @FieldType(
 *   id = "gcd_outpost_age_range_field",
 *   label = @Translation("Age range(s)"),
 *   description = @Translation("Computed age ranges from Outpost min and max values."),
 *   default_widget = "string_textfield",
 *   default_formatter = "string"
 * )
 */
final class OutpostAgeRangeFieldItem extends StringItem {
}
