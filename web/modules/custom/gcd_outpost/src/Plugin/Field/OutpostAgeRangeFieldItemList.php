<?php

namespace Drupal\gcd_outpost\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;

/**
 * Add values for computed age range field.
*/
class OutpostAgeRangeFieldItemList extends FieldItemList {
  use ComputedItemListTrait;

  /**
   * {@inheritdoc}
   */
  protected function computeValue() {
    $entity = $this->getEntity();

    $ranges = [];
    $ranges = $this->calculateRanges($entity->localgov_outpost_age_min->value ?? 0, $entity->localgov_outpost_age_max->value ?? 9999);
    foreach ($ranges as $delta => $range) {
      $item = $this->createItem($delta, ['value' =>$range]);
      $this->list[$delta] = $item;
    }
  }

  /**
   * Return appropriate ranges for min max values.
   */
  private function calculateRanges(int $min, int $max): array {
    $matching = [];
    $ranges = [
      '0-4 years' => [0, 4],
      '5-11 years' => [5, 11],
      '12-15 years' => [12, 15],
      '16-18 years' => [16, 18],
      '19-25 years' => [19, 25],
    ];

    foreach ($ranges as $name => $range) {
      if ($min <= $range[1] && $max >= $range[0]) {
        $matching[] = $name;
      }
    }

    return $matching;
  }
}
