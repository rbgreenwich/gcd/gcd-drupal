<?php

namespace Drupal\gcd_outpost\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Retrieves all 'Funded places for' active from Outpost Meta.
 *
 * @MigrateProcessPlugin(
 *   id = "gcd_outpost_meta_funded_places",
 *   handle_multiples = TRUE
 * )
 */
class MetaFundedPlaces extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_excutable, Row $row, $destination_property)  {
    $funded = [];
    foreach ($value as $key_value) {
      if (
        (substr(string: $key_value['key'], offset: 0, length: 17) == 'Funded places for')
        && ($key_value['value'] == 'Yes')
      ) {
        $funded[] = substr(string: $key_value['key'], offset: 17);
      }
    }

    return $funded;
  }
}
