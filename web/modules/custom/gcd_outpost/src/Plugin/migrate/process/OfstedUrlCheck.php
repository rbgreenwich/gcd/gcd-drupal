<?php

namespace Drupal\gcd_outpost\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Checks incoming Ofsted URL and tries to correct if needed.
 *
 * @MigrateProcessPlugin(
 *   id = "gcd_outpost_ofsted_url",
 *   handle_multiples = FALSE
 * )
 */
class OfstedUrlCheck extends ProcessPluginBase {

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_excutable, Row $row, $destination_property)  {
    // Only operate on invalid URLs that would otherwise prevent the whole row being imported.
    if (!filter_var($value, FILTER_VALIDATE_URL)) {
      $matches = [];
      // Early Years EY123456 URNs.
      if (preg_match('/\w{2}\d{6}/', $value, $matches)) {
        // We can fix because we know the URL format.
        $value = 'http://www.ofsted.gov.uk/inspection-reports/find-inspection-report/provider/CARE/' . reset($matches);
      }
      else {
        // Other URNs not covered, we'd need to know a URL format.
        // Let's not break the import.
        $this->messenger()->addError($this->t("Ofsted URL '@value' invalid.", ['@value' => $value]));
        $value = NULL;
      }
    }

    return $value;
  }
}
