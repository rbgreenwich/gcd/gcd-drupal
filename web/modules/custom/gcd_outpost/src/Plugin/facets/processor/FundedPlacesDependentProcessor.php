<?php

namespace Drupal\gcd_outpost\Plugin\facets\processor;

use Drupal\facets\FacetInterface;
use Drupal\facets\Processor\BuildProcessorInterface;
use Drupal\facets\Processor\ProcessorPluginBase;
use Drupal\facets\FacetManager\DefaultFacetManager;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\facets\Result\Result;
use Drupal\taxonomy\TermInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a processor that makes a facet depend on the state of another facet.
 *
 * Customised for specific case. It would have needed patching. And there was
 * nothing to handle children excepting listing and updating them manually. So
 * why not. And we can sort here too.
 *
 * @FacetsProcessor(
 *   id = "gcd_funded_places_dependent_processor",
 *   label = @Translation("Funded Places dependenies"),
 *   description = @Translation("Custom rules for when to display the facet."),
 *   stages = {
 *     "build" = 5
 *   }
 * )
 */
class FundedPlacesDependentProcessor extends ProcessorPluginBase implements BuildProcessorInterface, ContainerFactoryPluginInterface {

  const FIND_SUPPORT_CHILDREN = 743;
  const CATEGORY_CHILDCARE = 752;

  /**
   * Constructs a new object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\facets\FacetManager\DefaultFacetManager $facets_manager
   *   The language manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, protected DefaultFacetManager $facetsManager, protected EntityTypeManagerInterface $entityTypeManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('facets.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build(FacetInterface $facet, array $results): array {
    $facet_storage = $this->entityTypeManager->getStorage('facets_facet');

    // Find support for.
    $facet = $facet_storage->load('gcd_outpost_directory');
    $facet = $this->facetsManager->returnBuiltFacet($facet);
    if ($this->hasActive($facet, static::FIND_SUPPORT_CHILDREN)) {
      return $this->sortFacet($results);
    }

    // Category.
    $facet = $facet_storage->load('outpost_category_1');
    $facet = $this->facetsManager->returnBuiltFacet($facet);
    if ($this->hasActive($facet, static::CATEGORY_CHILDCARE)) {
      return $this->sortFacet($results);
    }

    return [];
  }

  /**
   * Is facet active, or is it the parent of the active term.
   */
  protected function hasActive(FacetInterface $facet, int $tid): bool {
    $term_storage = $this->entityTypeManager->getStorage('taxonomy_term');

    foreach ($facet->getActiveItems() as $value) {
      if ($value == $tid) {
        return TRUE;
      }
      // We're loading the terms elsewhere anyway.
      // Check the single direct parent.
      $term = $term_storage->load($value);
      if (($term instanceof TermInterface) &&
        ($term->parent->target_id == $tid)
      ) {
        return TRUE;
      }
    }

    return FALSE;
  }

  /**
   * Order by age.
   */
  protected function sortFacet(array $results): array {
    usort($results, function (Result $a, Result $b) {
      if (
        (strpos($a->getRawValue(), 'year') === FALSE) &&
        (strpos($b->getRawValue(), 'year') !== FALSE)
      ) {
        // Months smaller than years.
        return -1;
      }
      if (
        (strpos($a->getRawValue(), 'year') !== FALSE) &&
        (strpos($b->getRawValue(), 'year') === FALSE)
      ) {
        // Years bigger than months.
        return 1;
      }
      // Both either month or year.
      // Get first number (not digit).
      $a_value = $b_value = 0;
      $matches = [];
      if (preg_match('/\d+/', $a->getRawValue(), $matches)) {
        $a_value = $matches[0];
      }
      $matches = [];
      if (preg_match('/\d+/', $b->getRawValue(), $matches)) {
        $b_value = $matches[0];
      }
      // Same age. Put the longer ones later. Likely to be more specific.
      if ($a_value == $b_value) {
        return (strlen($a->getRawValue()) < strlen($b->getRawValue())) ? -1 : 1;
      }
      return ($a_value < $b_value) ? -1 : 1;
    });

    return $results;
  }

}


