<?php

declare(strict_types=1);

namespace Drupal\gcd_outpost\Drush\Commands;

use Drush\Attributes as CLI;
use Drupal\Core\Database\Connection;
use Drush\Commands\DrushCommands;
use Psr\Container\ContainerInterface as DrushContainer;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class GcdOutpostCommands extends DrushCommands {

  protected function __construct(
    protected Connection $database
  ) {
  }

  public static function create(ContainerInterface $container, DrushContainer $drush_container): self {
    return new static(
      $container->get('database')
    );
  }

  /**
   * Import a postcode to locality lookup CSV.
   */
  #[CLI\Command(name: 'gcd:import-localities-lookup', aliases: ['gcd:localities'])]
  #[CLI\Argument(name: 'path', description: 'Path to CSV file to import')]
  public function importLocalityLookup(string $path): void {
    $inserted = $no_post_code = $no_locality = $duplicate = 0;
    $this->logger()->notice($path);
    $this->database->delete('gcd_outpost_locality_lookup')->execute();
    if ($f = fopen($path, 'r')) {
      // Skip the field title line.
      fgets($f);
      while (($line = fgets($f)) !== FALSE) {
        list($post_code, $locality) = explode(',', $line);
        if ($post_code === '') {
          $no_post_code++;
        }
        elseif (($locality === '') || $locality[0] === '#') {
          $no_locality++;
        }
        else {
          $exists = $this->database
            ->select('gcd_outpost_locality_lookup')
            ->condition('post_code', $post_code)
            ->countQuery();
          if (!$exists->execute()->fetchField()) {
            $this->database
              ->insert('gcd_outpost_locality_lookup')
              ->fields(['post_code', 'locality'])
              ->values([$post_code, trim($locality)])
              ->execute();
            $inserted++;
          }
          else {
            $duplicate++;
          }
        }
      }
    }
    else {
      $this->logger()->error(\dt('Failed to open file.'));
    }

    $this->logger()->notice(\dt('Imported %inserted. Skipped: %duplicate duplicate, %no_post_code missing post code and %no_locality missing locality rows.', [
      '%inserted' => $inserted,
      '%duplicate' => $duplicate,
      '%no_post_code' => $no_post_code,
      '%no_locality' => $no_locality,
    ]));
  }

}
