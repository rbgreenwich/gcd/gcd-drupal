((Drupal) => {
  Drupal.behaviors.tabbedContent = {
    attach: (context, settings) => {
      const pager = document.querySelector(".rbg-pagination");

      window.tabbedContentOnTab = (tab) => {
        const facets = document.querySelectorAll(".facets-checkbox");
        for (const facet of facets) {
          const facetWrapper = facet.parentElement;
          const facetLink = facetWrapper.querySelector("a");
          const url = new URL(facetLink.href);
          url.searchParams.set("active_tab", tab);
          facetLink.href = url.toString();
        }
        if (pager) {
          if (tab === "map-view") {
            pager.classList.add("hidden");
          } else {
            pager.classList.remove("hidden");
          }
        }
      };
      const getActiveTabFromQueryString = () => {
        const url = new URL(window.location.href);
        const activeTab = url.searchParams.get("active_tab");
        return activeTab;
      };
      if (getActiveTabFromQueryString() === "map-view") {
        if (pager) {
          pager.classList.add("hidden");
        }
      }
    },
  };
})(Drupal);
