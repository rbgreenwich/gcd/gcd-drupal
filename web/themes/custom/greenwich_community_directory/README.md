# greenwich_community_directory #

Drupal theme for the Greenwich Community Directory, based on the [greenwich_base](https://gitlab.com/rbgreenwich/greenwich_base) theme and components.
