#!/usr/bin/env sh

# If settings.php for default profile does not exist, site install with drush is required
if [ ! -f /opt/drupal/"${SITE_NAME}"/web/sites/default/settings.php ];
then

  cd /opt/drupal/"${SITE_NAME}"

  # Drush status before site-install
  ./vendor/drush/drush/drush status

  # Copy Drupal config files to web/sites/default  (which might be mounted to a network drive)
  cp -R assets/composer/* web/sites/default/.

  # Echo settings which will be passed into drush site-install command
  echo 'Attempting drush site install with the following arguments: localgov' -vvv -y --db-url=mysql://"${DB_UNAME}":"${DB_PASSWORD}"@"${DB_HOST}":3306/"${DB_NAME}" --account-name="${ACCOUNT_UNAME}" --account-pass="${ACCOUNT_PASSWORD}" --site-name="${SITE_NAME}"

  # Install using localgov profile and configure website with database information
  ./vendor/drush/drush/drush site:install localgov -vvv -y --db-url=mysql://${DB_UNAME}:${DB_PASSWORD}@${DB_HOST}:3306/${DB_NAME} --account-name=${ACCOUNT_UNAME} --account-pass=${ACCOUNT_PASSWORD} --site-name=${SITE_NAME}

  # System site UUID should match the value found in system.site.yml within config/sync directory
  echo 'Attempting to set system site UUID to' ${SITE_UUID}
  ./vendor/drush/drush/drush config:set system.site uuid ${SITE_UUID} -y

  if [ $? == 0 ];
  then
    # Drush site-install succeeded

    # Set config item for css preprocessing
    ./vendor/drush/drush/drush -y config-set system.performance css.preprocess 0

    # Print Drush status after site-install
    ./vendor/drush/drush/drush status
  else
    # Drush site-install failed

    # Remove default.settings.php file
    rm web/sites/default/settings.php

    echo 'drush site-install failed. Removed drupal settings files ready for next attempt.'
    exit 1;
  fi
fi

# Copy settings.php to web/sites/default  (which might be mounted to a network drive)
chmod u+w /opt/drupal/"${SITE_NAME}"/web/sites/default/
chmod u+w /opt/drupal/"${SITE_NAME}"/web/sites/default/settings.php
cp /opt/drupal/"${SITE_NAME}"/assets/composer/settings.php /opt/drupal/"${SITE_NAME}"/web/sites/default/.
chmod u-w /opt/drupal/"${SITE_NAME}"/web/sites/default/
chmod u-w /opt/drupal/"${SITE_NAME}"/web/sites/default/settings.php

if [ -f /opt/drupal/"${SITE_NAME}"/web/sites/default/settings.php ];
then

  # Ensure write permission on the file upload directory
  chmod -R +w /opt/drupal/"${SITE_NAME}"/web/sites/default/files

  # Drush deploy to updatedb, clear caches and import config
  ./vendor/drush/drush/drush deploy -v -y

  # Change symlink for apache document root to LocalGov Drupal web root
  ln -sfn /opt/drupal/"${SITE_NAME}"/web /var/www/html

  echo "export SITE_NAME=${SITE_NAME}" >> /etc/apache2/envvars
  echo "export BASE_URL=${BASE_URL}" >> /etc/apache2/envvars
  echo "export PHP_UPLOAD_MAX_FILESIZE=${PHP_UPLOAD_MAX_FILESIZE}" >> /etc/apache2/envvars
  echo "export PHP_POST_MAX_SIZE=${PHP_POST_MAX_SIZE}" >> /etc/apache2/envvars
  echo "export PHP_MEMORY_LIMIT=${PHP_MEMORY_LIMIT}" >> /etc/apache2/envvars
  echo "export PHP_MAX_EXECUTION_TIME=${PHP_MAX_EXECUTION_TIME}" >> /etc/apache2/envvars
  echo "export DB_NAME=${DB_NAME}" >> /etc/apache2/envvars
  echo "export DB_USER=${DB_USER}" >> /etc/apache2/envvars
  echo "export DB_PASSWORD=${DB_PASSWORD}" >> /etc/apache2/envvars
  echo "export DB_HOST=${DB_HOST}" >> /etc/apache2/envvars
  echo "export DRUPAL_HASH_SALT=${DRUPAL_HASH_SALT}" >> /etc/apache2/envvars
  echo "export AWS_KEY=${AWS_KEY}" >> /etc/apache2/envvars
  echo "export AWS_SECRET=${AWS_SECRET}" >> /etc/apache2/envvars
  echo "export CLOUDFRONT_DISTRIBUTION_ID=${CLOUDFRONT_DISTRIBUTION_ID}" >> /etc/apache2/envvars

  # Restart apache to ensure new document root is respected
  service apache2 restart

  # Keep the container alive by tailing dev null
  tail -f /dev/null
fi
